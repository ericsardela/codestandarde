package code.standard;

public class AppDoCurso {
    public static void main( String[] args ){

        Pessoa jaum = new Pessoa();
        jaum.setIdadeDaPessoaAqui(50);
        jaum.setNomeDoIndividuo("João");

        System.out.println( "Oi, eu sou o" + jaum.getNomeDoIndividuo() + ", e tenho " + jaum.getIdadeDaPessoaAqui() + " anos." );
    }
}
