package code.standard;

public class Pessoa {
    private int idadeDaPessoaAqui;
    private String nomeDoIndividuo;

    public String getNomeDoIndividuo() {
        return nomeDoIndividuo;
    }

    public void setNomeDoIndividuo(String nomeDoIndividuo) {
        this.nomeDoIndividuo = nomeDoIndividuo;
    }

    public int getIdadeDaPessoaAqui() {
        return idadeDaPessoaAqui;
    }

    public void setIdadeDaPessoaAqui(int idadeDaPessoaAqui) {
        this.idadeDaPessoaAqui = idadeDaPessoaAqui;
    }
}
